# Django 在 [21云盒子](https://www.21cloudbox.com/) 的示例

这是 [21云盒子](http://www.21cloudbox.com/) 上创建的 [Django](https://www.djangoproject.com/) 示例。

## 部署

详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-django-project-in-production-server.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-django-project-in-production-server.html)